let collection = [];


function print(){
 if(collection.length > 0){
    return ('not empty')
} else { 
    return collection
}
}
// Write the queue functions below.
function enqueue(item){
	collection.push(item)

	return collection
	
} 

function dequeue(){
      collection.shift()
     return collection

     }

 function front(){
    return collection[0];

      }

function size() {
	return collection.length;
}

function isEmpty(){
	return collection.length === 0;
}

module.exports = {
	print,
	enqueue,
	dequeue,
	collection,
	front,
	size,
	isEmpty,

};